<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * Orders controller,
 * Reponsible for loading appropriate information from model and passing it to the view
 */
class Orders extends CI_Controller {

    public function index() {  // Default method for this controller
        browser();
    }

    /**
     * Browser method is called when users browse to orders/browser
     * Checks if user is logged in and if so, proceeds to display browser page
     * filtering by employees and/or customers if specified, if an employee is
     * not specified then the results are filtered by the currently logged in
     * employee.
     */
    public function browser() {

        $this->load->helper('url');
        $this->load->library('session');
        if (!$this->session->userdata('isLoggedIn')) {
            redirect("/", 307);
        }
        $this->load->model('order');
        $this->load->model('employee');
        $this->load->model('customer');
        $this->load->helper('url');

        
        $currentlySelectedEmployee = $this->input->get("employees");
        //If currently selected employee is not set, filter by logged in user
        if ($currentlySelectedEmployee === false) {
            //exit($this->session->userdata('userCode'));
            $currentlySelectedEmployee = $this->session->userdata('userCode');
        }

        $currentlySelectedCustomer = $this->input->get("customers");
        $orderData = $this->order->filterOrders($currentlySelectedEmployee, $currentlySelectedCustomer);

        $data['title'] = 'Order Browser';
        $data['orderData'] = $orderData;
        $data['employees'] = $this->employee->listAll();
        $data['customers'] = $this->customer->listAll();
        $data['currentlySelectedEmployee'] = $currentlySelectedEmployee;
        $data['currentlySelectedCustomer'] = $currentlySelectedCustomer;



        $data['content'] = $this->load->view(
                'orders/browser', $data, TRUE);
        $this->load->view('templates/master', $data);
    }

    /**
     * View is called when a user browses to the URL 'orders/view/$id where id
     * is the id of the order to be shown. If an id that does not exist in the 
     * orderdetails table is passed in for some reason a 404 page is displayed
     * @param type $id ID of the order/order details to be displayed
     */
    public function view($id) {
        $this->load->model('orderDetails');
        $this->load->model('order');
        $this->load->library('session');
        $this->load->helper('url');
        
        if (!$this->session->userdata('isLoggedIn')) {
            redirect("/", 307);
        }
        

        $orderDetails = $this->orderDetails->readOrders($id);

        if ($orderDetails === false) {
            show_404();
        }
        $data['title'] = "View Order";
        $data['orderDetails'] = $orderDetails;
        $data['order'] = $this->order->read($id);

        $data['content'] = $this->load->view(
                'orders/view', $data, TRUE);
        $this->load->view('templates/master', $data);
    }

}
