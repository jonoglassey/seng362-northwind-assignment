<?php

/**
 * Pages controller taken from earlier lab, allows users to view static pages
 * created in the project
 */
class Pages extends CI_Controller {

    /**
     * Loads a static page within the pages folder in views, this method is
     * called when user browses to /pages/$page where page is the name of the
     * page to be displayed, this name corresponds to the file name in the 
     * views/pages folder
     * @param type $page name of the page to be displayed, corresponds
     * to the name of the file in the views/pages 
     */
    public function view($page = 'about') {
        $this->load->helper('url');
        $this->load->library('session');


        if (!file_exists(APPPATH . '/views/pages/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $this->load->helper('html');
        $this->load->helper('url');
        $data['title'] = ucfirst($page);
        $data['content'] = $this->load->view('pages/' . $page, $data, TRUE);
        $this->load->view('templates/master', $data);
    }

}
