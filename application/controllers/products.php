<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Products Controller, Handles logic between models and views for the 
 * product browser
 */
class Products extends CI_Controller {

    /**
     * Default method for controller, redirects to browser by default
     */
    public function index() {  // Default method for this controller
        browser();
    }

    /**
     * Displays browser, this method is called when the user browses to
     * /products/browser, If the user is logged in, then the product browser 
     * is shown. This method also handles the filtering of the products list
     * according to the category
     */
    public function browser() {
        $this->load->model('category');
        $this->load->model('product');
        $this->load->helper('url');

        $this->load->helper('url');
        $this->load->library('session');
        if (!$this->session->userdata('isLoggedIn')) {
            redirect("/", 307);
        }


        $categoryMap = $this->category->listAll();
        $currentCategoryId = $this->input->get('Category');
        if (!$currentCategoryId) {
            $currentCategoryId = key($categoryMap);
        }
        $productMap = $this->product->listAll($currentCategoryId);
        $currentProductId = $this->input->get('Product');

        if (!$currentProductId) {
            $currentProductId = key($productMap);
        }

        $product = $this->product->read($currentProductId);

        if ($product->categoryID !== $currentCategoryId) {
            $currentProductId = key($productMap);
            $product = $this->product->read($currentProductId);
        }

        $title = 'Products Browser';
        $data = array('categoryMap' => $categoryMap,
            'productMap' => $productMap,
            'categoryId' => $currentCategoryId,
            'product' => $product,
            'title' => $title);
        $data['content'] = $this->load->view(
                'products/productBrowser', $data, TRUE);
        $this->load->view('templates/master', $data);
    }

}
