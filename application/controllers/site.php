<?php

/**
 * Site Controller handles site/session related transactions between the model 
 * and view, but is mainly used for logging in and out at the moment
 */
class Site extends CI_Controller {

    /**
     * Method called when user browses to /site/login, checks if the user
     * currently has as session or not, if so user is redirected to page
     * that is specified by redirectPage parameter otherwise, show_login method
     * is called to prompt user for login
     * @param type $redirectPage On a successful login, the page the user
     * will be redirected to
     */
    public function index($redirectPage = '/products/browser') {
        $this->load->library('session');
        $this->load->helper('url');

        $this->session->set_userdata(array(
            'redirectPage' => $redirectPage,
        ));
        if ($this->session->userdata('isLoggedIn')) {
            redirect($redirectPage);
        } else {
            $this->show_login();
        }
    }

    /**
     * Shows the login Page and displays any errors passed in
     * @param type $errors Errors to be displayed on login page
     */
    public function show_login($errors = array()) {
        $this->load->helper('url');
        $data['errors'] = $errors;
        $data['title'] = "Login";

        $this->load->helper('form');
        $data['content'] = $this->load->view('/login/index', $data, TRUE);
        $this->load->view('/templates/master', $data);
    }

    /**
     * Checks POSTed username and password is valid and then either redirects
     * to method to set sessoin variables on successful validatoin or calls
     * method to show loginpage again and passes error message to it
     */
    public function login_user() {
        $this->load->model('logindetails');
        $errors = array();

        $inputUsername = $this->input->post('username');
        $inputPassword = $this->input->post('password');

        $loginDetails = $this->logindetails->authorise($inputUsername, $inputPassword);


        if ($loginDetails !== false) {
            $this->set_session($loginDetails);
            $this->index($this->session->userdata('redirectPage'));
        } else {

            $errors[0] = "Please enter a valid Username and Password";
            $this->show_login($errors);
        }
    }

    /**
     * Sets session variables
     * @param type $user logindata model that contains information to be stored
     * in session
     */
    public function set_session($user) {
        $this->load->library('session');
        $this->session->set_userdata(array(
            'username' => $user->username,
            'userCode' => $user->employeeID,
            'isLoggedIn' => true,
        ));
    }

    /**
     * Destroys the currently active session
     */
    public function end_session() {
        $this->load->library('session');
        $this->load->helper('url');
        $this->session->sess_destroy();
        redirect("/");
    }

}
