<?php

/**
 * Customer class, stores informatoin for a single instance of a customer from
 * the Customers table in the database
 */
class Customer extends CI_Model {
    public $id;
    public $code;
    public $companyName;
    public $contactTitle;
    public $address;
    public $city;
    public $region;
    public $postalCode;
    public $country;
    public $phone;
    public $fax;
    
    /**
     * Default Constructor for Model
     */
    public function __construct() {
        $this->load->database();
    }
   

    /*
     * Return a Customer object read from the database for the given customer.
     * Throws an exception if no such customer exists in the database or if
     * there is more than one item with the same ID. Strings from database
     * are parsed with htmlspecialchars
     */
    public function read($id) {
        $cat = new Customer();
        $query = $this->db->get_where('Customers', array('id'=>$id));
        if ($query->num_rows !== 1) {
            throw new Exception("Customer ID $id not found in database");
        }

        // Copy all database column values into this, converting column names
        // to fields names by converting first char to lower case.
        $row = $query->result();
        $row = $row[0];
        foreach ((array) $row as $field=>$value) {
            $fieldName = strtolower($field[0]) . substr($field, 1);
            if(is_string($value)) {
                $value = htmlspecialchars($value);
            }
            $cat->$fieldName = $value;
        }
        return $cat;
    }


    /*
     * Return a map from CustomerID tp Customer Model for all customers
     */
    public function listAll() {
        $rows = $this->db->get('Customers')->result();  // Gets all rows
        $list = array();
        foreach ($rows as $row) {
            $list[$row->id] = $this->read($row->id);
        }
        return $list;
    }
    

};

