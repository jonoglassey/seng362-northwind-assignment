<?php

/**
 * Employee class, stores informatoin for a single instance of a employees from
 * the Employee table in the database
 */
class Employee extends CI_Model {
    public $id;
    public $lastName;
    public $firstName;
    public $title;
    public $titleOfCourtesy;
    public $birthDate;
    public $hireDate;
    public $address;
    public $city;
    public $region;
    public $postalCode;
    public $country;
    public $homePhone;
    public $extension;
    public $notes;
    public $reportsTo;
    
    public function __construct() {
        $this->load->database();
    }

    /*
     * Return a employee object read from the database for the given employee.
     * Throws an exception if no such customer exists in the database or if
     * there is more than one item with the same ID.
     */
    public function read($id) {
        $cat = new Employee();
        $query = $this->db->get_where('Employees', array('id'=>$id));
        if ($query->num_rows !== 1) {
            throw new Exception("Employee ID $id not found in database");
        }

        // Copy all database column values into this, converting column names
        // to fields names by converting first char to lower case.
        $row = $query->result();
        $row = $row[0];
        foreach ((array) $row as $field=>$value) {
            $fieldName = strtolower($field[0]) . substr($field, 1);
            if(is_string($value)) {
                $value = htmlspecialchars($value);
            }
            $cat->$fieldName = $value;
        }
        return $cat;
    }


    /*
     * Return a map from employeeID to Employee Model for all employees
     */
    public function listAll() {
        $rows = $this->db->get('Employees')->result();  // Gets all rows
        $list = array();
        foreach ($rows as $row) {
            $list[$row->id] = $this->read($row->id);
        }
        return $list;
    }
    
    public function getName() {
        return $this->firstName . " " . $this->lastName;
    }
    
};

