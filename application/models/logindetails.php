<?php

/**
 * Login Details class, stores methods for authenticating usernames and passwords
 * of users
 */
class LoginDetails extends CI_Model {

    public $id;
    public $employeeID;
    public $employeeModel;
    public $password;
    public $username;

    public function __construct() {
        $this->load->database();
        $this->load->model('employee');
    }

    /**
     * Attempts to assert whether or not a username and password appear in the
     * logindetails table, if they do the logindetails model is created, 
     * populated and returned otherwise false is returned
     * 
     * Password is converted into MD5 and compared against MD5 hash present
     * in database
     * @param type $username Username to be searched for in database
     * @param type $password Password to be hashed and searched for in database
     * @return boolean|\LoginDetails
     */
    public function authorise($username, $password) {
        $hash = md5($password);
        $query = $this->db->get_where("LoginDetails", array('password' => $hash, 'username' => $username));

        if ($query->num_rows !== 1) {
            return false;
        }

        $rows = $query->result();

        $loginDetails = new LoginDetails();
        $loginDetails->load($rows[0]);

        return $loginDetails;
    }

    /**
     * Loads logindetails model for a given row
     * @param type $row Query row to be turned into a logindetails object
     */
    private function load($row) {
        foreach ((array) $row as $field => $value) {
            $fieldName = strtolower($field[0]) . substr($field, 1);
            if (is_string($value)) {
                $value = htmlspecialchars($value);
            }
            $this->$fieldName = $value;
        }

        $this->employeeModel = $this->employee->read($this->employeeID);
    }

}

;

