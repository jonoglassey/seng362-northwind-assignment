<?php
class Order extends CI_Model {
    public $id;
    public $customerID;
    public $customerModel;
    public $employeeID;
    public $employeeModel;
    public $orderDate;
    public $requiredDate;
    public $shippedDate;
    public $shipVia;
    public $shipperModel;
    public $freight;

    public function __construct() {
        $this->load->database();
        $this->load->model('employee');
        $this->load->model('customer');
        $this->load->model('shipper');
    }

    /*
     * Return a Order object read from the database for the given order.
     * @param int $id  Id of the order to be returned. Returns false if no
     * such ID is found
     * @return a Order instance/False boolean
     */
    public function read($id) {
        $prod = new Order();
        
        $query = $this->db->get_where('Orders', array('id'=>$id));
        
        if ($query->num_rows !== 1) {
            return false;
        }

        $rows = $query->result();
        $row = $rows[0];
        $prod->load($row);
        return $prod;
    }


    /**
     * Filters orders by given arguements and returns array of orders as result
     * @param type $employeeId employeeID to filter by
     * @param type $customerId customerID to filter by
     * @return \Order
     */
    public function filterOrders($employeeId=NULL, $customerId=NULL) {
        $this->db->select('*');
        if ($employeeId) {
            $this->db->where(array('employeeID' => $employeeId));
        }
        if ($customerId) {
            $this->db->where(array('customerID' => $customerId));
        }
        $query = $this->db->get('Orders');
        $list = array();
        foreach ($query->result() as $row) {
            $order = new Order();
            $order->load($row);
            $list[$row->id] = $order;
        }
        //print_r($list);
        //exit("<br/>Done ");
        return $list;
    }




    // Given a row from the database, copy all database column values
    // into 'this', converting column names to fields names by converting
    // first char to lower case.
    private function load($row) {
        foreach ((array) $row as $field => $value) {
            $fieldName = strtolower($field[0]) . substr($field, 1);
            if(is_string($value)) {
                $value = htmlspecialchars($value);
            }
            $this->$fieldName = $value;
            
        }
        
        $this->customerModel = $this->customer->read($this->customerID);
        $this->employeeModel = $this->employee->read($this->employeeID);
        $this->shipperModel = $this->shipper->read($this->shipVia)
;        
    }


    // Check that the result from a DB query was OK
    private static function checkResult($result) {
        global $DB;
        if (!$result) {
            die("DB error ({$DB->error})");
        }
    }
    
    
};

