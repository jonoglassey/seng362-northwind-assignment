<?php

/**
 * Customer class, stores informatoin for a single instance of orderdetails from
 * the OrderDetails table in the database
 */
class OrderDetails extends CI_Model {
    public $id;
    public $orderId;
    public $orderModel;
    public $productID;
    public $productModel;
    public $quantity;
    public $discount;

    public function __construct() {
        $this->load->database();
        $this->load->model('product');
        $this->load->model('order');
    }

    /**
     * For a given id returns one or more orderdetails models in an array, if 
     * present, otherwise returns false
     * @param type $id ID of order for order details to be found for
     * @return \OrderDetails|boolean
     */
    public function readOrders($id) {
        
        
        $query = $this->db->get_where('OrderDetails', array('orderId'=>$id));
        
        if ($query->num_rows === 0) {
            return false;
        }

        $rows = $query->result();
        
        $orders = array();
        
        foreach($rows as $row) {
            $prod = new OrderDetails();
            $prod->load($row);
            $orders[] = $prod;
        }
        return $orders;
    }



    // Given a row from the database, copy all database column values
    // into 'this', converting column names to fields names by converting
    // first char to lower case.
    private function load($row) {
        foreach ((array) $row as $field => $value) {
            $fieldName = strtolower($field[0]) . substr($field, 1);
            if(is_string($value)) {
                $value = htmlspecialchars($value);
            }
            $this->$fieldName = $value;
            
        }
        
        $this->productModel = $this->product->read($this->productID);
        $this->orderModel = $this->order->read($this->orderId);
        
    }


    // Check that the result from a DB query was OK
    private static function checkResult($result) {
        global $DB;
        if (!$result) {
            die("DB error ({$DB->error})");
        }
    }
    
    
};

