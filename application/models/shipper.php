<?php

/**
 * Shipper class, stores informatoin for a single instance of a Shipper from
 * the Shippers table in the database
 */
class Shipper extends CI_Model {
    public $id;
    public $companyName;
    public $phone;
    
    public function __construct() {
        $this->load->database();
    }

    /*
     * Return a shipper object read from the database for the given shipper.
     * Throws an shipper if no such product exists in the database or if
     * there is more than one item with the same ID.
     */
    public function read($id) {
        $cat = new Shipper();
        $query = $this->db->get_where('Customers', array('id'=>$id));
        if ($query->num_rows !== 1) {
            throw new Exception("Customer ID $id not found in database");
        }

        // Copy all database column values into this, converting column names
        // to fields names by converting first char to lower case.
        $row = $query->result();
        $row = $row[0];
        foreach ((array) $row as $field=>$value) {
            $fieldName = strtolower($field[0]) . substr($field, 1);
            if(is_string($value)) {
                $value = htmlspecialchars($value);
            }
            $cat->$fieldName = $value;
        }
        return $cat;
    }


    /*
     * Return a map from Shipper to Shipper Model for all shippers
     */
    public function listAll() {
        $rows = $this->db->get('Shippers')->result();  // Gets all rows
        $list = array();
        foreach ($rows as $row) {
            $list[$row->id] = $this->read($row->id);
        }
        return $list;
    }
};

