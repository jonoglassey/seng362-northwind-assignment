<?php

/**
 * Supplier class, stores informatoin for a single instance of a Supplier from
 * the Suppliers table in the database
 */
class Supplier extends CI_Model {
    public $id;
    public $companyName;
    public $contactName;
    public $contactTitle;
    public $address;
    public $city;
    public $region;
    public $postalCode;
    public $country;
    public $phone;
    public $fax;
    public $description;
    
    public function __construct() {
        $this->load->database();
    }

    /**
     * Constructs a supplier model for a given ID
     * @param type $id id of supplier to be constructed
     * @return \Supplier
     * @throws Exception If supplier ID is not found in database
     */
    public function read($id) {
        $cat = new Supplier();
        $query = $this->db->get_where('Suppliers', array('id'=>$id));
        if ($query->num_rows !== 1) {
            throw new Exception("Supplier ID $id not found in database");
        }

        // Copy all database column values into this, converting column names
        // to fields names by converting first char to lower case.
        $row = $query->result();
        $row = $row[0];
        foreach ((array) $row as $field=>$value) {
            $fieldName = strtolower($field[0]) . substr($field, 1);
            if(is_string($value)) {
                $value = htmlspecialchars($value);
            }
            $cat->$fieldName = $value;
        }
        return $cat;
    }
};

