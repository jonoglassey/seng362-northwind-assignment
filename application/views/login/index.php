<h2>Login</h2>


<div class="col-md-4">
    <?php echo form_open('site/login_user') ?>
    <?php 

    foreach($errors as $error) { ?>
    <div class="alert alert-warning" role="alert">
        <?php echo $error; ?>
    </div>
    <?php }  ?>

    <input type="text" name="username" placeholder="Login" class="form-control" required>
    <input type="password" name="password" placeholder="Password" class="form-control" required>
    <input type="submit" class="btn btn-primary">

    </form>
</div>

