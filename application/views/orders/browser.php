<?php 

/**
 * Formats a Date to be of the format m-d-y (10-30-95), if date is equal to 0
 * then null is returned
 * @param type $date Date string to be formatted
 * @return Date/null
 */
function formatDate($date) {
    //exit($date);
    date_default_timezone_set('Pacific/Auckland');
    if($date != 0) {
        return date("m-d-y", strtotime($date));
    }
    
    return null;
}

/*
 * Return the HTML for a div containing a label, a combo box and a 'go'
 * button. 
 * @param string $label - text to display as a label
 * @param assoc-array $map - a map of ids to employees models
 * @param int $selectedRowValue - the value of the currently-selected option
 * @param int $size - the number of elements to display
 * @return - an html string for display
 */
function comboBoxEmployee($label, $map, $selectedRowValue, $size=1) {
    $html = "<div class='combobox'><span class='combo-label'>$label:</span> ";
    $html .= "<select id='$label' name='$label' size='$size'>";
    $html .= "<option value=''>All</option>";
    foreach ($map as $employee) {
        if ($employee->id === $selectedRowValue) {
            $selected = 'selected';
        } else {
            $selected = '';
        }
        $html .= "<option value='$employee->id' $selected>$employee->firstName $employee->lastName</option>\n";
    }
    $html .= "</select>\n";
    $html .= "<input type='submit' name='$label-submit' value='Go'>\n";
    $html .= "</div>";
    return $html;
}

/*
 * Return the HTML for a div containing a label, a combo box and a 'go'
 * button. 
 * @param string $label - text to display as a label
 * @param assoc-array $map - a map of ids to customer models
 * @param int $selectedRowValue - the value of the currently-selected option
 * @param int $size - the number of elements to display
 * @return - an html string for display
 */
function comboBoxCustomer($label, $map, $selectedRowValue, $size=1) {
    $html = "<div class='combobox'><span class='combo-label'>$label:</span> ";
    $html .= "<select id='$label' name='$label' size='$size'>";
    $html .= "<option value=''>All</option>";
    foreach ($map as $customer) {
        if ($customer->id === $selectedRowValue) {
            $selected = 'selected';
        } else {
            $selected = '';
        }
        $html .= "<option value='$customer->id' $selected>$customer->companyName</option>\n";
    }
    $html .= "</select>\n";
    $html .= "<input type='submit' name='$label-submit' value='Go'>\n";
    $html .= "</div>";
    return $html;
}

?>


<h2>Order Browser</h2>


<form id="browser-form" action="<?php echo site_url('orders/browser'); ?>" method="get">
    <?php echo comboBoxEmployee("employees", $employees, $currentlySelectedEmployee) ?>
    <?php echo comboBoxCustomer("customers", $customers, $currentlySelectedCustomer) ?>
</form>

        <table id='orderDetails' class="table">
            <tr>
                <th>Employee</th>
                <th>Customer</th>
                <th>Date Ordered</th>
                <th>Date Required</th>
                <th>Date Shipped</th>
                <th>More Info..</th>
            </tr>
            <?php foreach ((array) $orderData as $order) {
                //TODO: Revisit this, potentially a better way to do tihs
               // if(array_key_exists($field, $tableLabels)) {
                ?>

                <tr>
                    
                    <td><?php echo $order->employeeModel->firstName . " " . $order->employeeModel->lastName ?></td>
                    <td><?php echo $order->customerModel->companyName ?></td>
                    <td><?php echo formatDate($order->orderDate) ?></td>
                    <td><?php echo formatDate($order->requiredDate) ?></td>
                    <td><?php echo formatDate($order->shippedDate) ?></td>
                    <td><?php printf("<a href='%s'>More...</a>", site_url("/orders/view/$order->id")) ?> </td>
                </tr>
                <?php
             //   }
            }
            ?>
        </table>

