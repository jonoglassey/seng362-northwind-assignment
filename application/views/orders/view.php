<?php
/**
 * Formats a Date to be of the format m-d-y (10-30-95), if date is equal to 0
 * then null is returned
 * @param type $date Date string to be formatted
 * @return Date/null
 */
function formatDate($date) {
    //exit($date);
    date_default_timezone_set('Pacific/Auckland');
    if($date != 0) {
        return date("m-d-y", strtotime($date));
    }
    
    return null;
}

?>

<h2><?php echo "Order #$order->id"; ?></h2>

<div class="col-md-12">
    <h3>Order Details</h3>
    <table class="table">
        <tr>
            <th>Filled By</th>
            <th>Ordered By</th>
            <th>Date Ordered</th>
            <th>Date Required</th>
        </tr>
        <tr>
            <td><?php echo $order->employeeModel->getName(); ?></td>
            <td><?php echo $order->customerModel->companyName; ?> </td>
            <td><?php echo formatDate($order->orderDate); ?></td>
            <td><?php echo formatDate($order->requiredDate); ?></td>
        </tr>
    </table>
</div>
<div class="col-md-12">
    <table class="table">
        <tr>
            <th>Product</th>
            <th>Quantity</th>
            <th>Discount</th>
        </tr>
        <?php foreach($orderDetails as $details) { ?>
        <?php
        $catID = $details->productModel->categoryID;
        $prodID = $details->productID;
        ?>
        <tr>
            <td>
                <a href="<?php echo site_url(htmlspecialchars("/products/browser?Category=$catID&Product=$prodID")) ?>">
                    <?php echo $details->productModel->productName ?>
                </a>
            </td>
            <td><?php echo $details->quantity ?></td>
            <td><?php echo $details->discount ?></td>
        </tr>
        <?php } ?>
    </table>
</div>
<div class="col-md-6">
    <h3>Shipping Details</h3>
    <table class="table">
        <tr>
            <th>Freight Cost</th>
            <td>$<?php echo $order->freight ?>
        </tr>
        <tr>
            <th>Shipped On</th>
            <td><?php echo formatDate($order->shippedDate); ?> </td>
        </tr>
        <tr>
            <th>Shipping Company</th>
            <td><?php echo $order->shipperModel->companyName; ?></td>
        </tr>
        <tr>
            <th>Phone No.</th>
            <td><?php echo $order->shipperModel->phone; ?></td>
        </tr>
        
    </table>
</div>
<div class="col-md-6">
    <h3>Client Information</h3>
    <table class="table customerInfo">
        <tr>
            <td>Code</td>
            <td><?php echo $order->customerModel->code ?></td>
        </tr>
        <tr>
            <td>Company Name</td>
            <td><?php echo $order->customerModel->companyName ?></td>
        </tr>
        <tr>
            <td>Contact Title</td>
            <td><?php echo $order->customerModel->contactTitle ?></td>
        </tr>
        <tr>
            <td>Address</td>
            <td><?php echo $order->customerModel->address ?>,<br/> 
                <?php echo $order->customerModel->city ?>,<br/> 
                <?php echo $order->customerModel->region ?> <?php $order->customerModel->postalCode ?>,<br/>
                <?php echo $order->customerModel->country ?>
            </td>
        </tr>
        <tr>
            <td>Phone</td>
            <td><?php echo $order->customerModel->phone ?></td>
        </tr>
        <tr>
            <td>Fax</td>
            <td><?php echo $order->customerModel->fax ?></td>
        </tr>
        
    </table>
</div>
