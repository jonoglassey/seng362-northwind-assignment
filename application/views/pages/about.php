<h1>About Northwind</h1>

<div class="col-md-6">
    <h3>What is Northwind?</h3>
    Northwind is a website built as an assignment for SENG365 at the University of Canterbury


    <h3>Product Browser</h3>
    The product browser supplied is the same as the one constructed in the labs, albeit slightly CSSed up! Due to time constraints
    the structure and way I output the table on this page is different to the rest of the site, if I had more time 
    I would have changed this to be the same as other table structures.

    <h3>Order Browser</h3>

    The order browser is a place where users can reviewed ordered placed with Northwind, here they are displayed with a table
    containing a filtered search of placed orders, users may filter results by employee (whom took the order) and the customer
    (whom made the order with Northwind). I made these the filters as I believed these were the two most important pieces 
    of information someone filtering the orders list would want to filter by. <br/><br/>

    The order browser is a page where users can check the orders placed within Northwind. As the employee logging in
    is someone whom would potential take orders and enter them into the database, I have made it so that in a case
    no filter has been selected the default orders that are displayed are filtered by the logged in user. This
    can of course be changed by altering it in the dropdown menu above.
    <br/>
    <br/>

    Users are then able to click on more at the end of each result to be taken to a specific order page

    <h3>Order Page</h3>
    The order page displays a more specific breakdown of information for a particular order. Including any product(s) ordered
    Users may also click on the product name to be taken to the corresponding listing in the product browser

    <h3>Style Note</h3>
    Please note to see styled website please make sure you are connected to the internet, bootstrap css files are being included
    from their remote cdn
</div>
<div class="col-md-6">
    <h3>Login</h3>

    <p>Currently there are only three user records present in the LoginDetails table, therefore only three possible logins can be used
    </p>
    <table class="table">
        <tr>
            <th></th>
            <th>Username</th>
            <th>Password</th>
        </tr>
        <tr>
            <th>Nancy Davolio</th>
            <td>dav123</td>
            <td>password123</td>
        </tr>
        <tr>
            <th>Andrew Fuller</th>
            <td>ful321</td>
            <td>password321</td>
        </tr>
        <tr>
            <th>Janet Leverling</th>
            <td>lev83</td>
            <td>92(?8P!4\(cx=oA</td>
        </tr>
    </table>

    <h3>Logging In</h3>
    Initially users will not be able to access any of the pages of this website expect for the login and 
    about pages, in order to access the remaining pages, the user must log in using one of the
    usernames and passwords designated on this page. Please enter these at the Login page. 

    <h4>How it works</h4>
    As this web application is meant to be used by employees of Northwind, each employee <i>in an ideal world</i> would have
    their own login and password, these are stored in the LoginDetails table which holds a foreign reference to the id of the user
    in the employees table.
    <br/><br/>
    Passwords which are POSTed from the login form are converted to MD5 and compared against a hash stored in the database for
    security
    <br/><br/>
    One potential security weakness to note is the password is being posted in plaintext to the server with then converts it to an MD5 checksum, 
    there is the potential that the password could be intercepted if not posted by secure means (HTTPS etc)

    <h3>Known Issues</h3>
    <ul>
        <li>On the order page when loading an unfiltered list (ie. employee = all, customer = all)
            it can take a few seconds (1 ~ 3 seconds) to load, I believe this is due to the fact I load
            models that are related to a model (by foreign key) for example product->category stores a category model.
            Were I to have more time I would have liked to attempt to fix this by using more optimised queries such as 
            joins etc.
        </li>
        <li>
            After logging in if a user presses back, their session will still be active, but the login page will still be displayed
        </li>
        <li>If a client does not have a field filled in in one of the address fields, a blank line will be displayed where the
            field should be</li>
    </ul>
</div>