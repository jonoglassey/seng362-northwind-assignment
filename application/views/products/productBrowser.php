<?php
/*
 * A view of the product catalogue, with a select widget for product
 * category and a table of products in the currently-selected
 * category.
 * 
 * Input params: $categoryMap - an associative array mapping from category ID
 *                         to category name
 *               $productMap - an associative array mapping from product ID
 *                         to product name for all products in the current
 *                         category.
 *               $product - the currently selected product, an instance
 *                         of the model class Product. The ID and category
 *                         of this product tell us the currently selected
 *                         productId and categoryId.
 * 
 */

define('COMBO_SIZE', 10);  // Number of lines to display
$tableLabels = array("id"               => "Product ID",
                     "productName"      => "Product Name",
                     "supplierName"     => "Supplier",
                     "categoryName"     => "Category",
                     "quantityPerUnit"  => "Quantity per unit",
                     "unitPrice"        => "Unit Price",
                     "unitsInStock"     => "Units in stock",
                     "unitsOnOrder"     => "Units on Order",
                     "reorderLevel"     => "Reorder level",
                     "discontinued"     => "Discontinued");

/*
 * Return the HTML for a div containing a label, a combo box and a 'go'
 * button. 
 * @param string $label - text to display as a label
 * @param assoc-array $map - a map from value to name
 * @param int $selectedRowValue - the value of the currently-selected option
 * @param int $size - the number of elements to display
 * @return - an html string for display
 */
function comboBoxHtml($label, $map, $selectedRowValue, $size=1) {
    $html = "<div class='combobox'><span class='combo-label'>$label:</span> ";
    $html .= "<select id='$label' name='$label' size='$size'>";
    foreach ($map as $id => $name) {
        if ($id === intval($selectedRowValue)) {
            $selected = 'selected';
        } else {
            $selected = '';
        }
        $html .= "<option value='$id' $selected>$name</option>\n";
    }
    $html .= "</select>\n";
    $html .= "<input type='submit' name='$label-submit' value='Go'>\n";
    $html .= "</div>";
    return $html;
}

?>
<h1>Product Browser</h1>

<div class='product-browser col-md-12'>
    <form id="browser-form" class="col-md-12" action="<?php echo site_url('products/browser'); ?>" method="get">
        <?php
        echo comboBoxHtml('Category', $categoryMap, $categoryId, COMBO_SIZE);
        echo comboBoxHtml('Product',  $productMap,  $product->id, COMBO_SIZE);
        ?>
     </form>
        <div class='product-details col-md-12'>
        <h2>Product Details</h2>
        <table class="table">
            <?php foreach ((array) $product as $field => $value) {
                //TODO: Revisit this, potentially a better way to do tihs
                if(array_key_exists($field, $tableLabels)) {
                ?>
                <tr>
                    <td><?php echo $tableLabels[$field]; ?></td>
                    <td><?php echo $value; ?></td>
                </tr>
                <?php
                }
            }
            ?>
        </table>
        </div>
    
</div>

