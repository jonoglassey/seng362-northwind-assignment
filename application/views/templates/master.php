<!DOCTYPE html>

<html>
<head>
    <title><?php echo "Northwind || $title"; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="<?php echo base_url("styles/nwstyles.css"); ?>" rel="stylesheet" type="text/css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="container">
    <h1>Northwind</h1>
        <nav class="navbar navbar-default">
            <ul class="nav navbar-nav">
            <?php if ($this->session->userdata('isLoggedIn')) {  ?>
                <li><a href="<?php echo site_url('/products/browser') ?>">Product Browser</a></li>
                <li><a href="<?php echo site_url('/orders/browser') ?>">Order Browser</a></li>
                <li><a href="<?php echo site_url('/pages/about') ?>">About</a></li>
                <li><a href="<?php echo site_url('/site/logout') ?>">Logout</a></li>
            <?php } else { ?> 
                <li><a href="<?php echo site_url('/pages/about') ?>">About</a></li>
                <li><a href="<?php echo site_url('/site/login') ?>">Login</a></li>
           <?php } ?>
            </ul>
        </nav>
        <!-- site banner, menu etc would go here -->
        <?php echo $content; ?>
     
        <span class="footer col-md-12" style="font-weight:bold">&copy; 2014</span>
    </div>
</body>
</html>
